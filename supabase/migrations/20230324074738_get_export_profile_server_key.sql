drop view if exists "public"."export_profile";

create table "public"."server_key" (
    "server_key" text not null
);

alter table "public"."server_key" enable row level security;

CREATE UNIQUE INDEX server_key_pkey ON public.server_key USING btree (server_key);

alter table "public"."server_key" add constraint "server_key_pkey" PRIMARY KEY using index "server_key_pkey";

create or replace view "public"."export_profile" with (security_invoker) as SELECT seq_id.seq_id,
    profile.id,
    profile.passport_name,
    profile.chosen_name,
    profile.gender,
    profile.sweatshirt_size,
    profile.tshirt_size,
    profile.updated_at,
    profile_type.role,
    profile_type.committee,
    country.name AS country_name,
    country.alpha3 AS country_alpha3,
    users.email,
    users.email_confirmed_at
   FROM ((((profile
     JOIN seq_id ON ((profile.id = seq_id.id)))
     JOIN profile_type ON ((profile.id = profile_type.id)))
     JOIN auth.users ON ((profile.id = users.id)))
     LEFT JOIN country ON ((profile_type.country_alpha3 = country.alpha3)));

set check_function_bodies = off;

CREATE OR REPLACE FUNCTION public.get_export_profile(sk text)
 RETURNS SETOF export_profile
 LANGUAGE sql
 SECURITY DEFINER
AS $function$
  select seq_id.seq_id,
    profile.id,
    profile.passport_name,
    profile.chosen_name,
    profile.gender,
    profile.sweatshirt_size,
    profile.tshirt_size,
    profile.updated_at,
    profile_type.role,
    profile_type.committee,
    country.name AS country_name,
    country.alpha3 AS country_alpha3,
    users.email,
    users.email_confirmed_at
    from profile
    join seq_id on profile.id=seq_id.id
    join profile_type on profile.id=profile_type.id
    join auth.users on profile.id=auth.users.id
    left join country on profile_type.country_alpha3=country.alpha3
    where exists (select * from server_key where server_key.server_key=sk);
$function$
;
