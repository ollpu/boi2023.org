create or replace view "public"."export_profile" as  SELECT seq_id.seq_id,
    profile.id,
    profile.updated_at,
    profile.passport_name,
    profile.chosen_name,
    profile.gender,
    profile.sweatshirt_size,
    profile.tshirt_size,
    profile_type.role,
    profile_type.committee,
    country.name AS country_name,
    country.alpha3 AS country_alpha3,
    users.email,
    users.email_confirmed_at
   FROM ((((profile
     JOIN seq_id ON ((profile.id = seq_id.id)))
     JOIN profile_type ON ((profile.id = profile_type.id)))
     JOIN auth.users ON ((profile.id = users.id)))
     LEFT JOIN country ON ((profile_type.country_alpha3 = country.alpha3)));



