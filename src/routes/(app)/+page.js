import { fetchMarkdownPosts } from '$lib/utils';

export const load = async () => {
  const sortedPosts = await fetchMarkdownPosts();
  return { sortedPosts };
};
