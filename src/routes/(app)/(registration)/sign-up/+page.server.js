import { supabaseAdmin } from '$lib/server/db';
import { fail, redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

export const load = async (event) => {
  const { session } = await getSupabase(event);
  if (session) throw redirect(303, '/registration');
};

export const actions = {
  default: async (event) => {
    const { request } = event;
    const { supabaseClient } = await getSupabase(event);
    const formData = await request.formData();

    const email = formData.get('email');
    const password = formData.get('password');
    const key = formData.get('key');
    const policy = formData.get('policy');

    if (!policy) return fail(422, { email, key, error: 'You must accept the privacy policy.' });

    const { data, error: inviteKeyError } = await supabaseAdmin
      .from('invite_key')
      .select('role, country_alpha3, committee')
      .eq('key', key)
      .maybeSingle();
    if (inviteKeyError)
      return fail(inviteKeyError.status, { email, key, error: inviteKeyError.message });
    const role = data?.role;
    const country_alpha3 = data?.country_alpha3;
    const committee = data?.committee;
    if (!role) return fail(422, { email, key, error: 'Invalid invite key.' });

    const { error: signUpError } = await supabaseClient.auth.signUp({
      email,
      password,
      options: {
        data: {
          role,
          country_alpha3,
          committee,
        },
      },
    });
    if (signUpError) return fail(signUpError.status, { email, key, error: signUpError.message });

    throw redirect(303, `/sign-in?message=A confirmation link has been sent to ${email}.`);
  },
};
