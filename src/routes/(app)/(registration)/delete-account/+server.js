import { supabaseAdmin } from '$lib/server/db';
import { redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

export const GET = async (event) => {
  const { session } = await getSupabase(event);
  if (!session) throw redirect(303, '/sign-in');
  const id = session.user.id;
  await supabaseAdmin.auth.admin.deleteUser(id);
  throw redirect(303, `/sign-out`);
};
