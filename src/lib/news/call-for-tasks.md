---
title: Call for tasks
date: 2022-12-14
---

As usual, all members of the greater BOI community (except potential contestants) are welcome to submit a task; each participating country is expected to submit at least one such task. We welcome any tasks of various difficulty levels. An ideal submission contains:

- Author information: name, email, affiliation, country, and (where applicable) role in the national olympiad.
- Task statement in English, preferably in PDF.
- Desription of the intended solution(s) and alternatives/partial solutions, an assessment of the task’s difficulty level(s), and some thoughts about instance generation.
- Whether you are willing to join the problem development team.

Please propose tasks in separate emails to the BOinf Google group (one message = one task), formatted as [BOI23 Task proposal] My Exciting Task Name. Where it makes sense add a three-letter national identifier in parentheses, such as [BOI23 Task proposal] Fizz Buzz (DNK). If you do not have access to the Google group, send your proposal to thore@itu.dk. Please submit your task by 31 Jan 2023, preferably earlier.

Submitted tasks must be kept in strict confidence until the end of BOI'23; expect your proposal to be shared with the members of the BOInf Google Group. Accepted tasks are expected to be released under a Creative Commons license (CC BY-SA or similar) after the contest is over.
